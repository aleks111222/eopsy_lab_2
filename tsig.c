#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <errno.h>

#define WITH_SIGNALS

bool sigintReceived; // SIGINT signal flag
void sigTerm() {

	printf("\nchild[%d]: SIGTERM interrupt received...\n", getpid());
	fflush(stdout);
}

void sigintHandler() {	// SIGINT handler

	sigintReceived = true;
	printf("\nparent[%d]: SIGNINT interrupt received...\n", getpid());
}

int main()
{
    	int NUM_CHILD; // Number of children to create
		int NUM_DONE = 0; // Number of children executed properly

    	printf("Enter the number of processes you want to create : ");
    	scanf("%d", &NUM_CHILD); // Get the number of children to create from the user

		pid_t child; // Children pid
        pid_t children[NUM_CHILD]; // Children pid array
        int status; // Exit status

#ifdef	WITH_SIGNALS
		for(int j = 1; j < NSIG; j++) {

			signal(j, SIG_BLOCK);
		}
		sigset_t mask3; // masks to block signals
/*		sigfillset(&mask1); // mask blocking all signals
		sigemptyset(&mask2); // mask only for unblocking signals
		sigaddset(&mask2, SIGCHLD); // add SIGCHLD signal to the mask*/
		sigemptyset(&mask3);
		sigaddset(&mask3, SIGTERM);
		signal(SIGCHLD, SIG_DFL);
		signal(SIGINT, sigintHandler); // Initiate SIGINT signal handler

        for (int i = 0; i < NUM_CHILD; i++) { // Loop for creating all children

                switch (child = fork()) {

                    	case -1: // If the process could not create properly
               		fprintf(stderr, "child[%d]: Error while creating the child", getpid());
					for(int j = 0; j < i; j++) { // Kill all children created till this point
					
						kill(children[j], SIGTERM);
					}
               		_exit(1);


               
                        case 0: // If the process was properly created
					 // Block all signals
					for(int j = 1; j < NSIG; j++) {
						if (j != SIGTERM)
						signal(j, SIG_IGN);
					}
					
					signal(SIGTERM, sigTerm);

					printf("child[%d]: created properly \n", getpid());
					fflush(stdout);
					sleep(10);

					printf("child[%d]: executed properly\n", getpid());
					fflush(stdout);
					_exit(0);



				
                        default: // If the process is a parent
  					//sigprocmask(SIG_SETMASK, &mask1, NULL); // Block all signals
					//sigprocmask(SIG_UNBLOCK, &mask2, NULL); // Unblock SIGCHLD signal
				//	signal(SIGTERM, sigTerm);
                	children[i] = child;
				}
			//sigprocmask(SIG_UNBLOCK, &mask1, NULL); // Unblock all signals
			
			      	if(sigintReceived) { // If the interrupt was received
                				
                		for(int j = 0; j < i; j++) { // Terminating all processes that were created before the SIGINT was received
                			kill(children[j], SIGTERM);
                			printf("parent[%d]: sending SIGTERM signal to child[%d] \n",
                															getpid(), children[j]);
                			}
                		sigintReceived = false; // Allow next SIGINT
                	}
			

			sleep(1);
        }

        printf("parent[%d]: all child processes initialised properly...\n", getppid());
				        
		for(int i = 0; i < NUM_CHILD; i++) { // Terminate all properly executed processes
			waitpid(children[i], &status, 0);
			if(WIFEXITED(status) != 0) NUM_DONE++; // Count all legitimate processes
			kill(children[i], SIGTERM);
		}
				        		
		printf("parent[%d]: there are no more child processes...\n", getppid());
		printf("parent[%d]: number of executed child processes: %d \n", getppid(), NUM_DONE); 
		//sigprocmask(SIG_UNBLOCK, &mask1, NULL); // Unblock all signals

		_exit(0);
		
        
#endif

	for (int i = 0; i < NUM_CHILD; i++) {

                switch (child = fork()) {
                        case -1:
               		printf("child[%d]: Error while creating the child", getpid());
					for(int j = 0; j < i; j++) {
						kill(children[j], SIGTERM);
						kill(children[j], SIGKILL);
					}
                	_exit(1);
                        case 0:
					printf("child[%d]: created properly \n", getpid());
					fflush(stdout);
					sleep(10);
					printf("child[%d]: executed properly \n", getpid());
					fflush(stdout);
					_exit(0);
                        default:

                	children[i] = child;
                	sleep(1);
						
       			 }
		}
        
        printf("parent[%d]: all child processes initialised properly\n", getppid());
        
       	for(int i = 0; i < NUM_CHILD; i++) {
        	waitpid(children[i], &status, 0);
        	if(WIFEXITED(status) != 0) NUM_DONE++;
        	kill(children[i], SIGTERM);
        }
        		
       	printf("parent[%d]: there are no more child processes...\n", getppid());
        printf("parent[%d]: number of executed child processes: %d \n", getppid(), NUM_DONE);       
}
